package com.dsd10.backup;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileFilter;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.UnknownHostException;
import java.nio.ByteBuffer;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.io.comparator.LastModifiedFileComparator;
import org.apache.commons.io.filefilter.FileFileFilter;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.dsd10.enums.FileState;
import com.dsd10.enums.TypeSyncFromBackup;
import com.dsd10.models.FileSync;
import com.dsd10.utils.DataInput;
import com.dsd10.utils.DataOutput;

public class ServiceBackup {

	public static int PORT = 9696;
	public static String IPSERVER = "127.0.0.1";
	public static File[] listFileNow;
	public static long lastSync;
	static ServerSocket server;

	/**
	 * @param args
	 * @throws IOException
	 */
	public static void main(String[] args) throws IOException {
		Path path = Paths.get("config.txt");
		try {
			byte[] bConfig = Files.readAllBytes(path);
			String config = new String(bConfig);
			System.out.println(config);
			JSONObject joConfig = new JSONObject(config);
			if (!joConfig.isNull("last_sync"))
				lastSync = joConfig.getLong("last_sync");
			else
				lastSync = 0L;
			if (!joConfig.isNull("port"))
				PORT = joConfig.getInt("port");
			if (!joConfig.isNull("ipserver"))
				IPSERVER = joConfig.getString("ipserver");

		} catch (JSONException | IOException e1) {

		}
		try {
			server = new ServerSocket(PORT);
		} catch (IOException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}
		while (true) {
			try {
				Socket socket = server.accept();
				System.out.println("Accepted connection : " + socket);
				SyncThread thread = new SyncThread(socket);
				thread.start();
			} catch (IOException e) {
				server = null;
				e.printStackTrace();
			}
		}

	}

}

class SyncThread extends Thread {
	Socket socket;
	private final InputStream mmInStream;
	private final OutputStream mmOutStream;
	private static ArrayList<ByteArrayOutputStream> listBytesAttack = new ArrayList<ByteArrayOutputStream>();
	public static String DIR = "../DataBackup";
	public static File[] listFileNow;
	public static long lastSync;

	public SyncThread(Socket socket) {
		Path path = Paths.get("config.txt");
		try {
			byte[] bConfig = Files.readAllBytes(path);
			String config = new String(bConfig);
			System.out.println(config);
			JSONObject joConfig = new JSONObject(config);
			if (!joConfig.isNull("last_sync"))
				lastSync = joConfig.getLong("last_sync");
			else
				lastSync = 0L;
			if (!joConfig.isNull("dir"))
				DIR = joConfig.getString("dir");

		} catch (JSONException | IOException e1) {

		}
		this.socket = socket;
		InputStream tmpIn = null;
		OutputStream tmpOut = null;
		try {
			tmpIn = socket.getInputStream();
			tmpOut = socket.getOutputStream();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		mmInStream = tmpIn;
		mmOutStream = tmpOut;

	}

	@Override
	public void run() {
		ByteArrayOutputStream bytesJson = new ByteArrayOutputStream();
		ArrayList<FileSync> listFile = new ArrayList<FileSync>();

		JSONObject json = new JSONObject();
		boolean isFinish = false;
		int BYTEREAD = 4096;
		int bytes;
		int maxBytesJson = 0;
		int maxBytes = 0;
		byte[] buffer = new byte[BYTEREAD];
		boolean isReadJsonFinish = false;
		int indexListFile = 0;
		int type = TypeSyncFromBackup.NONE;
		while (true) {
			if (isFinish)
				continue;
			try {
				if (type == TypeSyncFromBackup.NONE) {
					type = mmInStream.read(buffer, 0, 4);
					if (type <= 0) {
						type = TypeSyncFromBackup.NONE;
						continue;
					}
					type = ByteBuffer.wrap(buffer).getInt();
					switch (type) {
					case TypeSyncFromBackup.INFO_FILE_BACKUP:
						type = TypeSyncFromBackup.NONE;
						write(getByteFile());
						continue;
					case TypeSyncFromBackup.SERVER_GET_FILE_BACKUP:
						type = TypeSyncFromBackup.NONE;
						try {
							write(displayFiles());
						} catch (JSONException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
							cancel(false);
						}
						continue;
					case TypeSyncFromBackup.SYNC_FILE:
						break;
					case TypeSyncFromBackup.STOP_THREAD:
						this.socket.close();
						this.stop();
						break;
					default:
						break;
					}
				}
				if (maxBytes == 0) {
					maxBytes = 1;
					maxBytesJson = mmInStream.read(buffer, 0, 4);
					if (maxBytesJson <= 0)
						continue;
					maxBytesJson = ByteBuffer.wrap(buffer).getInt();
					System.out.println(maxBytesJson);
					// get JSON
				}
				if (!isReadJsonFinish) {
					if ((bytes = mmInStream.read(buffer, 0, ((maxBytesJson
							- bytesJson.size() > BYTEREAD) ? BYTEREAD
							: maxBytesJson - bytesJson.size()))) >= 0
							&& bytesJson.size() < maxBytesJson) {
						bytesJson.write(buffer, 0, bytes);
						if (bytesJson.size() >= maxBytesJson) {
							isReadJsonFinish = true;
							try {
								String joo = new String(bytesJson.toByteArray());
								System.out.println("JSON: " + joo);

								json = new JSONObject(joo);
								listFile = getListFile(json);
								System.out.println("Service "
										+ "ListFile Size: " + listFile.size());
							} catch (JSONException e) {
								e.printStackTrace();
								isFinish = true;
								continue;
							}
						}
					}
				} else {
					if (indexListFile < listFile.size()) {
						System.out.println("index: " + indexListFile);
						if (listFile.get(indexListFile).getState() == FileState.DELETE) {
							indexListFile += 1;
						} else {
							int offsetByte = (int) listFile.get(indexListFile)
									.getSize()
									- listBytesAttack.get(indexListFile).size();
							if (offsetByte > 0
									&& (bytes = mmInStream.read(buffer, 0,
											((offsetByte > BYTEREAD) ? BYTEREAD
													: offsetByte))) > 0) {
								listBytesAttack.get(indexListFile).write(
										buffer, 0, bytes);
								// sizeNow += bytes;
								// DataOutput da = new DataOutput();
								// da.writeInt((int)(sizeNow * 100.0) /
								// maxBytes);
								// write(da.getBytes());
								// mHandler.obtainMessage(MESSAGE_UPDATE_PROCESS,
								// maxBytes, -1, (int)(sizeNow * 100.0/
								// maxBytes)).sendToTarget();
								// Log.d("Percent receive Attack ",
								// indexListAttack
								// + " " + sizeNow);
								System.out
										.println(listBytesAttack.get(
												indexListFile).size()
												+ "  "
												+ listFile.get(indexListFile)
														.getSize());
								if (listBytesAttack.get(indexListFile).size() >= listFile
										.get(indexListFile).getSize()) {
									indexListFile += 1;
								}
							} else
								indexListFile += 1;
						}
						if (indexListFile >= listFile.size()) {
							maxBytes = 0;
							isReadJsonFinish = false;
							for (int i = 0; i < listFile.size(); i++) {
								System.out.println("Save File: " + listFile.get(i).getFilename() + "  "+ listBytesAttack
										.get(i).size());
								File f = new File(DIR,listFile.get(i)
										.getFilename());
								if (listFile.get(i).getState() == FileState.DELETE) {
									System.out.println(f.delete());
								} else {
									try{
										FileOutputStream fos = new FileOutputStream(f);
										fos.write(listBytesAttack
												.get(i).toByteArray());
										fos.flush();
										fos.close();
									}catch(FileNotFoundException ef){
										ef.printStackTrace();
									}
								}
							}
							System.out.println("Finish Sync");
							Path path = Paths.get("config.txt");
							byte[] bConfig = Files.readAllBytes(path);
							String config = new String(bConfig);
							System.out.println(config);
							JSONObject joConfig = new JSONObject();
							try {
								joConfig = new JSONObject(config);
							} catch (JSONException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
								joConfig = new JSONObject();
							}
							try{
								File fiCongif = new File(".","config.txt");
								joConfig.remove("last_sync");
								joConfig.put("last_sync", System.currentTimeMillis());
								if(joConfig.has("files"))
									joConfig.remove("files");
								joConfig.put("files",ListFileJSON(getListFileNow()));
								FileOutputStream fos = new FileOutputStream(fiCongif);
								fos.write(joConfig.toString().getBytes());
								fos.flush();
								fos.close();
							}catch(JSONException e){
							}
							cancel(true);
						}
					} else {
						System.out.println("Sync Error maxIndex");
						cancel(false);
					}
				}
			} catch (IOException e) {
				e.printStackTrace();
				System.out.println("Sync Error " + e.getMessage());
				cancel(false);
				break;
			}
		}
	}

	public byte[] getByteFile() throws IOException {
		JSONObject joSend = new JSONObject();
		try {
			joSend.put("last_sync", lastSync);
			listFileNow = getListFileNow();
			joSend.put("files", ListFileJSON(listFileNow));
		} catch (JSONException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		DataOutput dataSend = new DataOutput();
		dataSend.writeInt(TypeSyncFromBackup.INFO_FILE_BACKUP);
		dataSend.writeInt(joSend.toString().getBytes().length);
		dataSend.write(joSend.toString().getBytes());
		return dataSend.getBytes();
	}

	public synchronized void write(byte[] buffer) {
		try {
			mmOutStream.write(buffer, 0, buffer.length);
			mmOutStream.flush();

			// Share the sent message back to the UI Activity
			// mHandler.obtainMessage(BluetoothChat.MESSAGE_WRITE, -1, -1,
			// buffer).sendToTarget();
		} catch (IOException e) {
		}
	}

	private static ArrayList<FileSync> getListFile(JSONObject json)
			throws JSONException {
		ArrayList<FileSync> listFile = new ArrayList<FileSync>();
		int size = json.getInt("size");
		JSONArray ja = json.getJSONArray("files");
		for (int i = 0; i < size; i++) {
			FileSync file = new FileSync();
			file.putData(ja.getJSONObject(i));
			listFile.add(file);
			ByteArrayOutputStream outp = new ByteArrayOutputStream();
			listBytesAttack.add(outp);
		}
		return listFile;
	}

	public static JSONArray ListFileJSON(File[] files) {
		JSONArray ja = new JSONArray();
		for (File file : files) {
			if (!file.getName().equals("config.txt"))
				ja.put(file.getName());
		}
		return ja;
	}
	
	public static byte[] displayFiles() throws JSONException, IOException {

		Path path = Paths.get("config.txt");
		byte[] bConfig = Files.readAllBytes(path);
//		File f = new File("config.txt");
//		ByteArrayOutputStream confiOut = new ByteArrayOutputStream();
//		FileInputStream fi = new FileInputStream(f);
//		byte[] byteConfig = new byte[4096];
//		int bytess = 0;
//		while((bytess = fi.read(byteConfig)) > 0){
//			confiOut.write(byteConfig, 0, bytess);
//		}
//		byte[] bConfig = confiOut.toByteArray();
		String config = new String(bConfig);
		System.out.println(config);
		JSONObject joo = new JSONObject();
		try {
			joo = new JSONObject(config);
		}catch(JSONException ej){
			
		}
		
		long lastSync = joo.getLong("last_sync");
		JSONArray jaFile = joo.getJSONArray("files");
		List<String> listFileName = new ArrayList<String>();
		for (int i = 0; i < jaFile.length(); i++) {
			listFileName.add(jaFile.getString(i));
		}
		File dir = new File(DIR);

		long nowSync = System.currentTimeMillis();
		File[] files = dir.listFiles((FileFilter) FileFileFilter.FILE);
		// displayFiles(files);
		//
		// new Thread(new SyncThread(socket)).start();

		Arrays.sort(files, LastModifiedFileComparator.LASTMODIFIED_REVERSE);
		System.out
				.println("\nLast Modified Ascending Order (LASTMODIFIED_COMPARATOR)");

		DataOutput totalByte = new DataOutput();
		JSONObject joAllFile = new JSONObject();
		JSONArray ja = new JSONArray();
		File[] fileBackup = new File[files.length];
		int positionBackup = 0;
		for (File file : files) {
			boolean isExist = false;
			if (listFileName.size() > 0)
				isExist = listFileName.remove(file.getName());
			if (!isExist) {
				System.out.printf(
						"File: %-20s Last Modified:" + file.lastModified()
								+ "\n", file.getName());
				FileSync fileSync = new FileSync();
				fileSync.setFilename(file.getName());
				fileSync.setDir(file.getParent());
				fileSync.setDatecreate(file.lastModified());
				fileSync.setState(FileState.CREATE);
				fileSync.setSize(file.length());
				ja.put(fileSync.toJSON());
				fileBackup[positionBackup] = file;
				positionBackup++;
			} else if (file.lastModified() >= lastSync) {
				System.out.printf(
						"File: %-20s Last Modified:" + file.lastModified()
								+ "\n", file.getName());
				FileSync fileSync = new FileSync();
				fileSync.setFilename(file.getName());
				fileSync.setDir(file.getParent());
				fileSync.setDatecreate(file.lastModified());
				if (isExist)
					fileSync.setState(FileState.CHANGE);
				else
					fileSync.setState(FileState.CREATE);
				fileSync.setSize(file.length());
				ja.put(fileSync.toJSON());
				fileBackup[positionBackup] = file;
				positionBackup++;
			} else {

			}
		}
		System.out.println("List File Delete: " + listFileName);
		for (String filename : listFileName) {
			FileSync filess = new FileSync();
			filess.setFilename(filename);
			filess.setState(FileState.DELETE);
			ja.put(filess.toJSON());
		}
		joAllFile.put("files", ja);
		joAllFile.put("size", ja.length());
		System.out.println(joAllFile.toString() + " "
				+ joAllFile.toString().getBytes().length);
		totalByte.writeInt(TypeSyncFromBackup.SERVER_GET_FILE_BACKUP);
		totalByte.writeInt(joAllFile.toString().getBytes().length);
		totalByte.write(joAllFile.toString().getBytes());
		for (int i = 0;i < positionBackup; i++) {
			File file = fileBackup[i];
			byte[] bytes = new byte[(int) file.length()];
			try {
				BufferedInputStream buf = new BufferedInputStream(
						new FileInputStream(file));
				buf.read(bytes, 0, bytes.length);
				buf.close();
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			totalByte.write(bytes);
		}
		return totalByte.getBytes();
	}

	private static File[] getListFileNow() {
		File dir = new File(DIR);
		File[] files = dir.listFiles((FileFilter) FileFileFilter.FILE);
		Arrays.sort(files, LastModifiedFileComparator.LASTMODIFIED_REVERSE);
		return files;
	}
	
	public void cancel(boolean isFinish){
		DataOutput finish = new DataOutput();
		finish.writeInt(TypeSyncFromBackup.STOP_THREAD);
		if(isFinish)
			finish.writeInt(TypeSyncFromBackup.THREAD_FINISH);
		else
			finish.writeInt(TypeSyncFromBackup.THREAD_NOT_FINISH);
		write(finish.getBytes());
		try {
			socket.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		this.stop();
	}
}
