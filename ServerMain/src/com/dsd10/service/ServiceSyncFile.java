package com.dsd10.service;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileFilter;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.UnknownHostException;
import java.nio.ByteBuffer;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.io.comparator.LastModifiedFileComparator;
import org.apache.commons.io.filefilter.FileFileFilter;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.dsd10.enums.FileState;
import com.dsd10.enums.TypeSyncFromBackup;
import com.dsd10.models.FileSync;
import com.dsd10.utils.DataOutput;

public class ServiceSyncFile {
	public static int PORT = 69;
	public static int PORTBACKUP = 9696;
	public static String IPBACKUP = "127.0.0.1";
	static ServerSocket server;
	static Socket socket;
	public static JSONObject joConfig = new JSONObject();
	public static List<String> listFileOldName = new ArrayList<String>();
	public static List<String> listFileNowName = new ArrayList<String>();
	public static long lastSync;
	public static long nowSync;

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Path path = Paths.get("config.txt");
		JSONObject joConfig = new JSONObject();
		try {
			byte[] bConfig = Files.readAllBytes(path);
			String config = new String(bConfig);
			System.out.println(config);
			joConfig = new JSONObject(config);
			if (!joConfig.isNull("port"))
				PORT = joConfig.getInt("port");
			if (!joConfig.isNull("port_backup"))
				PORTBACKUP = joConfig.getInt("port_backup");
			if (!joConfig.isNull("ipbackup"))
				IPBACKUP = joConfig.getString("ipbackup");
			if (!joConfig.isNull("last_sync"))
				lastSync = joConfig.getLong("last_sync");
			else
				lastSync = 0L;
		} catch (IOException | JSONException e3) {
			// TODO Auto-generated catch block
			e3.printStackTrace();
		}
		try {
			server = new ServerSocket(PORT);
		} catch (IOException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}
		if (lastSync <= 0) {
			try {
				Socket socket01 = new Socket(IPBACKUP, PORTBACKUP);
				BackupThread bThread = new BackupThread(socket01,System.currentTimeMillis());
				bThread.start();
				DataOutput dao = new DataOutput();
				dao.writeInt(TypeSyncFromBackup.INFO_FILE_BACKUP);
				bThread.write(dao.getBytes());
			} catch (UnknownHostException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}else{
			try {
				Socket socket01 = new Socket(IPBACKUP, PORTBACKUP);
				GetBackupThread bThread = new GetBackupThread(socket01);
				bThread.start();
				DataOutput dao = new DataOutput();
				dao.writeInt(TypeSyncFromBackup.SERVER_GET_FILE_BACKUP);
				bThread.write(dao.getBytes());
			} catch (UnknownHostException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
		while (true) {
			try {
				Socket socket = server.accept();
				System.out.println("Accepted PHP connection : " + socket);
				SyncThread thread = new SyncThread(socket);
				thread.start();
			} catch (IOException e) {
				server = null;
				e.printStackTrace();
			}

		}
	}

}

class BackupThread extends Thread {

	Socket socket;
	private final InputStream mmInStream;
	private final java.io.OutputStream mmOutStream;
	public static String DIR = "../DataServer";
	private long timeBackup;
	public BackupThread(Socket socket, long timeBackup) {
		Path path = Paths.get("config.txt");
		try {
			byte[] bConfig = Files.readAllBytes(path);
			String config = new String(bConfig);
			System.out.println(config);
			JSONObject joConfig = new JSONObject(config);
			if (!joConfig.isNull("dir"))
				DIR = joConfig.getString("dir");

		} catch (JSONException | IOException e1) {

		}
		InputStream tmpIn = null;
		OutputStream tmpOut = null;
		try {
			tmpIn = socket.getInputStream();
			tmpOut = socket.getOutputStream();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		this.timeBackup = timeBackup;
		this.socket = socket;
		mmInStream = tmpIn;
		mmOutStream = tmpOut;
	}

	@Override
	public void run() {
		ByteArrayOutputStream bytesJson = new ByteArrayOutputStream();
		JSONObject json = new JSONObject();
		boolean isFinish = false;
		int BYTEREAD = 4096;
		int bytes;
		int maxBytesJson = 0;
		int maxBytes = 0;
		byte[] buffer = new byte[BYTEREAD];
		boolean isReadJsonFinish = false;
		int indexListFile = 0;
		int type = TypeSyncFromBackup.NONE;
		while (true) {
			try {
				if (type == TypeSyncFromBackup.NONE) {
					type = mmInStream.read(buffer, 0, 4);
					if (type <= 0) {
						type = TypeSyncFromBackup.NONE;
						continue;
					}
					type = ByteBuffer.wrap(buffer).getInt();
					switch (type) {
					case TypeSyncFromBackup.INFO_FILE_BACKUP:
						break;
					case TypeSyncFromBackup.STOP_THREAD:
						Path path = Paths.get("config.txt");
						JSONObject joConfig = new JSONObject();
						try {
							byte[] bConfig = Files.readAllBytes(path);
							String config = new String(bConfig);
							System.out.println(config);
							joConfig = new JSONObject(config);

						} catch (JSONException | IOException e1) {
							joConfig = new JSONObject();
						}
						joConfig.remove("last_sync");
						try {
							joConfig.put("last_sync", timeBackup);
							File fiCongif = new File(".","config.txt");
							FileOutputStream fos = new FileOutputStream(fiCongif);
							fos.write(joConfig.toString().getBytes());
							fos.flush();
							fos.close();
						} catch (JSONException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						
						socket.close();
						this.stop();
						continue;
					default:
						break;
					}
				}
				if (maxBytes == 0) {
					maxBytes = 1;
					maxBytesJson = mmInStream.read(buffer, 0, 4);
					if (maxBytesJson <= 0)
						continue;
					maxBytesJson = ByteBuffer.wrap(buffer).getInt();
					System.out.println(maxBytesJson);
					// get JSON
				}
				if (!isReadJsonFinish) {
					if ((bytes = mmInStream.read(buffer, 0, ((maxBytesJson
							- bytesJson.size() > BYTEREAD) ? BYTEREAD
							: maxBytesJson - bytesJson.size()))) >= 0
							&& bytesJson.size() < maxBytesJson) {
						bytesJson.write(buffer, 0, bytes);
						if (bytesJson.size() >= maxBytesJson) {
							isReadJsonFinish = true;
							type = TypeSyncFromBackup.NONE;
							try {
								String joo = new String(bytesJson.toByteArray());
								System.out.println("BackupThread JSON: " + joo);

								json = new JSONObject(joo);
							} catch (JSONException e) {
								e.printStackTrace();
								isFinish = true;
								continue;
							}
							try {
								write(displayFiles(json));
							} catch (JSONException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
								this.socket.close();
								this.stop();
							}
						}
					}
				}
			} catch (IOException e) {
				// TODO Auto-generated catch block
				this.stop();
				e.printStackTrace();
			}
		}
	}

	public synchronized void write(byte[] buffer) {
		try {
			mmOutStream.write(buffer, 0, buffer.length);
			mmOutStream.flush();

			// Share the sent message back to the UI Activity
			// mHandler.obtainMessage(BluetoothChat.MESSAGE_WRITE, -1, -1,
			// buffer).sendToTarget();
		} catch (IOException e) {
		}
	}

	public void cancel() {

	}

	public static byte[] displayFiles(JSONObject joo) throws JSONException {

		long lastSync = joo.getLong("last_sync");
		JSONArray jaFile = joo.getJSONArray("files");
		List<String> listFileName = new ArrayList<String>();
		for (int i = 0; i < jaFile.length(); i++) {
			listFileName.add(jaFile.getString(i));
		}
		File dir = new File(DIR);

		long nowSync = System.currentTimeMillis();
		File[] files = dir.listFiles((FileFilter) FileFileFilter.FILE);
		// displayFiles(files);
		//
		// new Thread(new SyncThread(socket)).start();

		Arrays.sort(files, LastModifiedFileComparator.LASTMODIFIED_REVERSE);
		System.out
				.println("\nLast Modified Ascending Order (LASTMODIFIED_COMPARATOR)");

		DataOutput totalByte = new DataOutput();
		JSONObject joAllFile = new JSONObject();
		JSONArray ja = new JSONArray();
		File[] fileBackup = new File[files.length];
		int positionBackup = 0;
		for (File file : files) {
			boolean isExist = false;
			if (listFileName.size() > 0)
				isExist = listFileName.remove(file.getName());
			if (!isExist) {
				System.out.printf(
						"File: %-20s Last Modified:" + file.lastModified()
								+ "\n", file.getName());
				FileSync fileSync = new FileSync();
				fileSync.setFilename(file.getName());
				fileSync.setDir(file.getParent());
				fileSync.setDatecreate(file.lastModified());
				fileSync.setState(FileState.CREATE);
				fileSync.setSize(file.length());
				ja.put(fileSync.toJSON());
				fileBackup[positionBackup] = file;
				positionBackup++;
			} else if (file.lastModified() >= lastSync) {
				System.out.printf(
						"File: %-20s Last Modified:" + file.lastModified()
								+ "\n", file.getName());
				FileSync fileSync = new FileSync();
				fileSync.setFilename(file.getName());
				fileSync.setDir(file.getParent());
				fileSync.setDatecreate(file.lastModified());
				if (isExist)
					fileSync.setState(FileState.CHANGE);
				else
					fileSync.setState(FileState.CREATE);
				fileSync.setSize(file.length());
				ja.put(fileSync.toJSON());
				fileBackup[positionBackup] = file;
				positionBackup++;
			} else {

			}
		}
		System.out.println("List File Delete: " + listFileName);
		for (String filename : listFileName) {
			FileSync filess = new FileSync();
			filess.setFilename(filename);
			filess.setState(FileState.DELETE);
			ja.put(filess.toJSON());
		}
		joAllFile.put("files", ja);
		joAllFile.put("size", ja.length());
		System.out.println(joAllFile.toString() + " "
				+ joAllFile.toString().getBytes().length);
		totalByte.writeInt(TypeSyncFromBackup.SYNC_FILE);
		totalByte.writeInt(joAllFile.toString().getBytes().length);
		totalByte.write(joAllFile.toString().getBytes());
		for (int i = 0; i < positionBackup; i++) {
			File file = fileBackup[i];
			byte[] bytes = new byte[(int) file.length()];
			try {
				BufferedInputStream buf = new BufferedInputStream(
						new FileInputStream(file));
				buf.read(bytes, 0, bytes.length);
				buf.close();
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			totalByte.write(bytes);
		}
		return totalByte.getBytes();
	}
}

class GetBackupThread extends Thread {
	Socket socket;
	private final InputStream mmInStream;
	private final OutputStream mmOutStream;
	private static ArrayList<ByteArrayOutputStream> listBytesAttack = new ArrayList<ByteArrayOutputStream>();
	public static String DIR = "../DataServer";
	public static File[] listFileNow;
	public static long lastSync;

	public GetBackupThread(Socket socket) {
		Path path = Paths.get("config.txt");
		try {
			byte[] bConfig = Files.readAllBytes(path);
			String config = new String(bConfig);
			System.out.println(config);
			JSONObject joConfig = new JSONObject(config);
			if (!joConfig.isNull("last_sync"))
				lastSync = joConfig.getLong("last_sync");
			else
				lastSync = 0L;
			if (!joConfig.isNull("dir"))
				DIR = joConfig.getString("dir");

		} catch (JSONException | IOException e1) {

		}
		this.socket = socket;
		InputStream tmpIn = null;
		OutputStream tmpOut = null;
		try {
			tmpIn = socket.getInputStream();
			tmpOut = socket.getOutputStream();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		mmInStream = tmpIn;
		mmOutStream = tmpOut;

	}

	@Override
	public void run() {
		ByteArrayOutputStream bytesJson = new ByteArrayOutputStream();
		ArrayList<FileSync> listFile = new ArrayList<FileSync>();

		JSONObject json = new JSONObject();
		boolean isFinish = false;
		int BYTEREAD = 4096;
		int bytes;
		int maxBytesJson = 0;
		int maxBytes = 0;
		byte[] buffer = new byte[BYTEREAD];
		boolean isReadJsonFinish = false;
		int indexListFile = 0;
		int type = TypeSyncFromBackup.NONE;
		while (true) {
			if (isFinish)
				continue;
			try {
				if (type == TypeSyncFromBackup.NONE) {
					type = mmInStream.read(buffer, 0, 4);
					if (type <= 0) {
						type = TypeSyncFromBackup.NONE;
						continue;
					}
					type = ByteBuffer.wrap(buffer).getInt();
					switch (type) {
					case TypeSyncFromBackup.SERVER_GET_FILE_BACKUP:
						break;
					case TypeSyncFromBackup.STOP_THREAD:
						socket.close();
						stop();
						break;
					default:
						break;
					}
				}
				if (maxBytes == 0) {
					maxBytes = 1;
					maxBytesJson = mmInStream.read(buffer, 0, 4);
					if (maxBytesJson <= 0)
						continue;
					maxBytesJson = ByteBuffer.wrap(buffer).getInt();
					System.out.println(maxBytesJson);
					// get JSON
				}
				if (!isReadJsonFinish) {
					if ((bytes = mmInStream.read(buffer, 0, ((maxBytesJson
							- bytesJson.size() > BYTEREAD) ? BYTEREAD
							: maxBytesJson - bytesJson.size()))) >= 0
							&& bytesJson.size() < maxBytesJson) {
						bytesJson.write(buffer, 0, bytes);
						if (bytesJson.size() >= maxBytesJson) {
							isReadJsonFinish = true;
							try {
								String joo = new String(bytesJson.toByteArray());
								System.out.println("GetBackupThread JSON: " + joo);

								json = new JSONObject(joo);
								listFile = getListFile(json);
								System.out.println("GetBackupThread Service "
										+ "ListFile Size: " + listFile.size());
							} catch (JSONException e) {
								e.printStackTrace();
								isFinish = true;
								continue;
							}
						}
					}
				} else {
					if (indexListFile < listFile.size()) {
						System.out.println("GetBackupThread index: " + indexListFile);
						if (listFile.get(indexListFile).getState() == FileState.DELETE) {
							indexListFile += 1;
						} else {
							int offsetByte = (int) listFile.get(indexListFile)
									.getSize()
									- listBytesAttack.get(indexListFile).size();
							if (offsetByte > 0
									&& (bytes = mmInStream.read(buffer, 0,
											((offsetByte > BYTEREAD) ? BYTEREAD
													: offsetByte))) > 0) {
								listBytesAttack.get(indexListFile).write(
										buffer, 0, bytes);
								System.out
										.println(listBytesAttack.get(
												indexListFile).size()
												+ "  "
												+ listFile.get(indexListFile)
														.getSize());
								if (listBytesAttack.get(indexListFile).size() >= listFile
										.get(indexListFile).getSize()) {
									indexListFile += 1;
								}
							} else
								indexListFile += 1;
						}
						if (indexListFile >= listFile.size()) {
							maxBytes = 0;
							isReadJsonFinish = false;
							for (int i = 0; i < listFile.size(); i++) {
								System.out.println("GetBackupThread Save File: "
										+ listFile.get(i).getFilename() + "  "
										+ listBytesAttack.get(i).size());
								File f = new File(DIR, listFile.get(i)
										.getFilename());
								if (listFile.get(i).getState() == FileState.DELETE) {
									System.out.println(f.delete());
								} else {
									try {
										FileOutputStream fos = new FileOutputStream(
												f);
										fos.write(listBytesAttack.get(i)
												.toByteArray());
										fos.flush();
										fos.close();
									} catch (FileNotFoundException ef) {
										ef.printStackTrace();
									}
								}
							}
							System.out.println("Finish Sync");
							Path path = Paths.get("config.txt");
							byte[] bConfig = Files.readAllBytes(path);
							String config = new String(bConfig);
							System.out.println(config);
							try {
								JSONObject joConfig = new JSONObject(config);
								File fiCongif = new File(".", "config.txt");
								joConfig.remove("last_sync");
								joConfig.put("last_sync",
										System.currentTimeMillis());
								FileOutputStream fos = new FileOutputStream(
										fiCongif);
								fos.write(joConfig.toString().getBytes());
								fos.flush();
								fos.close();
							} catch (JSONException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}

							cancel();
						}
					} else {
						System.out.println("Sync Error maxIndex");
						cancel();
					}
				}
			} catch (IOException e) {
				e.printStackTrace();
				System.out.println("Sync Error " + e.getMessage());
				cancel();
				break;
			}
		}
	}

	public byte[] getByteFile() throws IOException {
		JSONObject joSend = new JSONObject();
		try {
			joSend.put("last_sync", lastSync);
			listFileNow = getListFileNow();
			joSend.put("files", ListFileJSON(listFileNow));
		} catch (JSONException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		DataOutput dataSend = new DataOutput();
		dataSend.writeInt(TypeSyncFromBackup.INFO_FILE_BACKUP);
		dataSend.writeInt(joSend.toString().getBytes().length);
		dataSend.write(joSend.toString().getBytes());
		return dataSend.getBytes();
	}

	public synchronized void write(byte[] buffer) {
		try {
			mmOutStream.write(buffer, 0, buffer.length);
			mmOutStream.flush();

			// Share the sent message back to the UI Activity
			// mHandler.obtainMessage(BluetoothChat.MESSAGE_WRITE, -1, -1,
			// buffer).sendToTarget();
		} catch (IOException e) {
		}
	}

	private static ArrayList<FileSync> getListFile(JSONObject json)
			throws JSONException {
		ArrayList<FileSync> listFile = new ArrayList<FileSync>();
		int size = json.getInt("size");
		JSONArray ja = json.getJSONArray("files");
		for (int i = 0; i < size; i++) {
			FileSync file = new FileSync();
			file.putData(ja.getJSONObject(i));
			listFile.add(file);
			ByteArrayOutputStream outp = new ByteArrayOutputStream();
			listBytesAttack.add(outp);
		}
		return listFile;
	}

	public static JSONArray ListFileJSON(File[] files) {
		JSONArray ja = new JSONArray();
		for (File file : files) {
			if (!file.getName().equals("config.txt"))
				ja.put(file.getName());
		}
		return ja;
	}

	public static byte[] displayFiles(File[] files) throws JSONException {
		DataOutput totalByte = new DataOutput();
		JSONObject joAllFile = new JSONObject();
		JSONArray ja = new JSONArray();
		for (File file : files) {

		}
		joAllFile.put("files", ja);
		joAllFile.put("size", ja.length());
		System.out.println(joAllFile.toString() + " "
				+ joAllFile.toString().getBytes().length);
		totalByte.writeInt(joAllFile.toString().getBytes().length);
		totalByte.write(joAllFile.toString().getBytes());
		for (File file : files) {
			if (file.lastModified() >= 0L) {
				byte[] bytes = new byte[(int) file.length()];
				try {
					BufferedInputStream buf = new BufferedInputStream(
							new FileInputStream(file));
					buf.read(bytes, 0, bytes.length);
					buf.close();
				} catch (FileNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				totalByte.write(bytes);
			} else {
				break;
			}
		}
		return totalByte.getBytes();
	}

	private static File[] getListFileNow() {
		File dir = new File(DIR);
		File[] files = dir.listFiles((FileFilter) FileFileFilter.FILE);
		Arrays.sort(files, LastModifiedFileComparator.LASTMODIFIED_REVERSE);
		return files;
	}

	public void cancel() {
		DataOutput finish = new DataOutput();
		finish.writeInt(TypeSyncFromBackup.STOP_THREAD);
		write(finish.getBytes());
		try {
			socket.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		this.stop();
	}
}

class SyncThread extends Thread {

	public static String IPBACKUP = "127.0.0.1";
	public static int PORT = 6969;
	Socket socket;
	private final InputStream mmInStream;
	private final java.io.OutputStream mmOutStream;

	public SyncThread(Socket socket) {
		Path path = Paths.get("config.txt");
		try {
			byte[] bConfig = Files.readAllBytes(path);
			String config = new String(bConfig);
			System.out.println(config);
			JSONObject joConfig = new JSONObject(config);
			if (!joConfig.isNull("port_backup"))
				PORT = joConfig.getInt("port_backup");
			if (!joConfig.isNull("ipbackup"))
				IPBACKUP = joConfig.getString("ipbackup");
		} catch (JSONException | IOException e1) {

		}
		InputStream tmpIn = null;
		OutputStream tmpOut = null;
		try {
			tmpIn = socket.getInputStream();
			tmpOut = socket.getOutputStream();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		this.socket = socket;
		mmInStream = tmpIn;
		mmOutStream = tmpOut;
	}

	@Override
	public void run() {
		int maxBytes = 0;
		byte[] buffer = new byte[4068];
		while (true) {
			try {
				maxBytes = mmInStream.read(buffer, 0, 4);

				if (maxBytes <= 0)
					continue;
				System.out.println(maxBytes + "  ");
				int typeMess = 0;
				String mss = new String(buffer);
				if (mss.startsWith("6969"))
					typeMess = 6969;
				else if (maxBytes <= 4)
					typeMess = ByteBuffer.wrap(buffer).getInt();
				switch (typeMess) {
				case TypeSyncFromBackup.INFO_FILE_BACKUP:

					break;
				case TypeSyncFromBackup.PHP_FILE:
					System.out.println("PHP File");
					try {
						Socket socket01 = new Socket(IPBACKUP, PORT);
						BackupThread bThread = new BackupThread(socket01,System.currentTimeMillis());
						bThread.start();
						DataOutput dao = new DataOutput();
						dao.writeInt(TypeSyncFromBackup.INFO_FILE_BACKUP);
						bThread.write(dao.getBytes());
					} catch (UnknownHostException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					} catch (IOException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					this.socket.close();
					this.stop();
					break;
				case TypeSyncFromBackup.STOP_THREAD:
					System.out.println("STOP THREAD");
					this.socket.close();
					this.stop();
					break;
				default:
					break;
				}
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	public synchronized void write(byte[] buffer) {
		try {
			mmOutStream.write(buffer, 0, buffer.length);
			mmOutStream.flush();

			// Share the sent message back to the UI Activity
			// mHandler.obtainMessage(BluetoothChat.MESSAGE_WRITE, -1, -1,
			// buffer).sendToTarget();
		} catch (IOException e) {
		}
	}

	public void cancel() {

	}
}
