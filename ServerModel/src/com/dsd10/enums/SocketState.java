package com.dsd10.enums;

public class SocketState {
	public static int NONE = 0;
	public static int OFFLINE = 1;
	public static int PREPARE_CONNECT = 2;
	public static int CONNECTED = 3;
	public static int CHECKING_CONNECTION_ALIVE = 4;
	public static int DISCONNECTED = 5;
}
