package com.dsd10.enums;

public class FileState {
	public static int CREATE = 0;
	public static int DELETE = 1;
	public static int CHANGE = 2;
	public static int MOVE = 3;
}
