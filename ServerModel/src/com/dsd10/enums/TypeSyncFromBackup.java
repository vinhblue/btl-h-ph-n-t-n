package com.dsd10.enums;

public class TypeSyncFromBackup {
	public static final int INFO_FILE_BACKUP = 1;
	public static final int SYNC_FILE = 2;
	public static final int PHP_FILE = 6969;
	public static final int SERVER_GET_FILE_BACKUP = 3;
	public static final int STOP_THREAD = 13;
	public static final int NONE = 0;
	public static final int THREAD_FINISH = 1;
	public static final int THREAD_NOT_FINISH = 2;
}
