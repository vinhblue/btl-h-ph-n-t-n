package com.dsd10.models;

import org.json.JSONException;
import org.json.JSONObject;

import com.dsd10.enums.FileState;

public class FileSync {
	
	private String filename;
	private String dir;
	private long size;
	private int type;
	private long datecreate;
	private int state;
	public String getFilename() {
		return filename;
	}
	
	public JSONObject toJSON() throws JSONException{
		JSONObject jo = new JSONObject();
		jo.put("filename", this.filename);
		jo.put("dir", this.dir);
		jo.put("size", this.size);
		jo.put("type", this.type);
		jo.put("datecreate", this.datecreate);
		jo.put("state",this.state);
		return jo;
	}
	
	public void putData(JSONObject json) throws JSONException{
		if(!json.isNull("filename"))
			this.filename = json.getString("filename");
		if(!json.isNull("dir"))
			this.dir = json.getString("dir");
		if(!json.isNull("size"))
			this.size = json.getLong("size");
		if(!json.isNull("type"))
			this.type = json.getInt("type");
		if(!json.isNull("dateacreate"))
			this.datecreate = json.getLong("datecreate");
		if(!json.isNull("state"))
			this.state = json.getInt("state");
	}
	
	public FileSync() {
		super();
		this.filename = "";
		this.dir = "";
		this.size = 0;
		this.type = 0;
		this.datecreate = 0;
		this.state = FileState.CREATE;
	}
	
	public FileSync(String filename, String dir, int size, int type,
			long datecreate, int state) {
		super();
		this.filename = filename;
		this.dir = dir;
		this.size = size;
		this.type = type;
		this.datecreate = datecreate;
		this.state = state;
	}



	public void setFilename(String filename) {
		this.filename = filename;
	}
	public String getDir() {
		return dir;
	}
	public void setDir(String dir) {
		this.dir = dir;
	}
	public long getSize() {
		return size;
	}
	public void setSize(long size) {
		this.size = size;
	}
	public int getType() {
		return type;
	}
	public void setType(int type) {
		this.type = type;
	}
	public long getDatecreate() {
		return datecreate;
	}
	public void setDatecreate(long datecreate) {
		this.datecreate = datecreate;
	}
	public int getState() {
		return state;
	}
	public void setState(int state) {
		this.state = state;
	}

}
